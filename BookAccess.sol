pragma solidity ^0.5.10;
pragma experimental ABIEncoderV2;


contract Balance {
    address[] users;
    address ContractOwner;
    
    event NewUser(uint256 NewUserID, address NewUserAddress);
    event AccessGranted(address TheUser, uint256 BookId);
    event AccessRevoked(address TheUser, uint256 BookId);
    
    mapping(address => mapping(uint256 => bool)) userBookAccess;
    
    uint256 userCounter;
    uint256 bookAccessCounter;
    
    
    modifier onlyOwner () {
        bool found = false;
        if(ContractOwner == msg.sender)
        if (!found) {
            revert('Message sender not a participant');
        }
        _;
    }
    
    constructor(address[] memory usersEntry) public {
        for (uint i=0; i<usersEntry.length; i++) {
            users.push(usersEntry[i]);
        }
        users.push(msg.sender);
        userCounter = 0;
        ContractOwner = msg.sender;
        bookAccessCounter = 0;
        
    }
    
    function newUser(address _newUser) public onlyOwner returns (bool)  {
        bool newEntry = true;
        for (uint i = 0; i<=users.length; i++) {
            if (users[i] == _newUser) {
                newEntry = false;
                revert('User already registered');
            }
        }
        if (newEntry) {
             users.push(_newUser);
             userCounter++;
             emit NewUser(userCounter, _newUser);
        }
        return newEntry;
    }
    
    function GrantAccess(address User, uint256 BookID) public onlyOwner returns(bool) {
        userBookAccess[User][BookID] = true;
        emit AccessGranted(User, BookID);
        return true;
    }
    
    function RevokeAccess(address User, uint256 BookID) public onlyOwner returns(bool) {
        userBookAccess[User][BookID] = false;
        emit AccessRevoked(User, BookID);
        return true;
    }
        
    function getUserAccess(address User, uint256 BookID) public view onlyOwner returns (bool)  {
        return (userBookAccess[User][BookID]);
    }
    
}
