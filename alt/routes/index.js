var express = require('express');
var request = require('request');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Atlas Library System', success: false, errors: req.session.errors });
  req.session.errors = null;
});

// router.post('https://beta-api.ethvigil.com/v0.1/contract/0x263fd33d2c2ed57cc20c549c562ed9047cd82901/mint', json=method_args, headers=headers, function (error, response, body) {
//     if (!error && response.statusCode == 200) {
//         console.log(body);
//         req.session.success = true;
//     }
//     if(error) {
//       req.session.errors = errors;
//       req.session.success = false;
//     }
//     res.redirect('/');
// })

function transact (address, amount) {
  var tHash = null;
  var contract_address = "0x263fd33d2c2ed57cc20c549c562ed9047cd82901";
  var api_key = 'c1894213-57fe-4cd0-9e4c-86c7cb15e4f7';
  var rest_api_endpoint = 'https://beta-api.ethvigil.com/v0.1';
  var method_args = {'account': address, 'amount': amount};
  var headers = {'accept': 'application/json', 'Content-Type': 'application/json', 'X-API-KEY': api_key};
  var method_api_endpoint = rest_api_endpoint+'/contract/'+contract_address+'/mint';
  console.log('Calling mint()........');
  console.log(`Contract: contract_address`);
  console.log(`Method arguments:\n===============\n{method_args}`);

  request.post({
    "headers": headers,
    "url": method_api_endpoint,
    "body": JSON.stringify(method_args)
  }, (error, response, body) => {
    if(error) {
        return console.dir(error);
    }
    else{
    //console.dir(JSON.parse(body));
    var incoming = JSON.parse(body);
    tHash = incoming.data[0].txHash;
    console.log(tHash);
    }
    return tHash;
  });
}


router.post('/submit', function(req, res, next) {
  var userAddress = req.body.address;
  var userAmt = req.body.amt;
  var txHash = transact(userAddress, userAmt);
  res.redirect('/test');
});

module.exports = router;
